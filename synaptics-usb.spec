%global tarvers 1.5rc4

Name:		synaptics-usb
Version:	1.5
Release:	4%{?tarvers:.%{tarvers}}%{?dist}
Summary:	USB Synaptics device driver - scripts and user interface

Group:		System Environment/Kernel
License:	GPLv2+
URL:		http://www.jan-steinhoff.de/linux/synaptics-usb.html
Source0:	http://www.jan-steinhoff.de/linux/%{name}-%{tarvers}.tar.bz2
Source10:	%{name}-README.fedora
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildARch:	noarch

Requires:	synaptics-usb-kmod >= %{version}
Provides:	synaptics-usb-kmod-common = %{version}

# For rebinding of kernel USB drivers
Requires:	kernel >= 2.6.13


%description
USB Synaptics device driver - scripts and user interface


%prep
%setup -q -n %{name}


%build
%{__cp} -p %{SOURCE10} README.fedora


%install
%{__rm} -rf "%{buildroot}"

%{__install} -d -m 0755 "%{buildroot}%{_sbindir}"
%{__install} -p -m 0755 synaptics-usb "%{buildroot}%{_sbindir}/synaptics-usb"
%{__install} -p -m 0755 synaptics-usb-rebind "%{buildroot}%{_sbindir}/synaptics-usb-rebind"
sed -i 's|/usr/local/sbin/|%{_sbindir}/|g' "%{buildroot}%{_sbindir}/synaptics-usb-rebind"


%clean
%{__rm} -rf "%{buildroot}"


%files
%defattr(-,root,root,-)
%doc README.fedora
%{_sbindir}/synaptics-usb
%{_sbindir}/synaptics-usb-rebind


%changelog
* Sun Apr 12 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 1.5-4.1.5rc4
- first working package

* Sun Apr 12 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 1.5-3.1.5rc4
- fix provides: name

* Sun Apr 12 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 1.5-2.1.5rc4
- require appropriate kernel package
- fix kmod requirement

* Sun Apr 12 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 1.5-1.1.5rc4
- initial package

